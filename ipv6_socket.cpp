#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdexcept>
#include <sstream>
#include <errno.h>
#include <string.h>
#include <iostream>

#include "ipv6_socket.h"


IPv6Description::IPv6Description(int port) :
	addr {
		AF_INET6,
		htons(port),
		0,
		IN6ADDR_ANY_INIT,
		0
	}
{	
}

IPv6Description::IPv6Description(std::string ip_addr, int port) :
	addr {
		AF_INET6,
		htons(port),
		0,
		IN6ADDR_ANY_INIT,
		0
	}
{
	throw std::logic_error(
		"IPv6Description::IPv6Description(std::string ip_addr, int "
		"port) not implemented yet");
	// TODO: build address from string
}

IPv6Description::~IPv6Description()
{
}

sockaddr_in6 * IPv6Description::get_sockaddr()
{
	return &addr;
}



ConnectionBinder::ConnectionBinder()
{
}

void ConnectionBinder::accepted(int fd, sockaddr_in6 &t)
{
	throw std::logic_error(
		"ConnectionBinder::accepted() may not be called. "
		"Make a sublass, please.");
}



IPv6ListenSocket::IPv6ListenSocket(
	IPv6Description & ip,
	int backlog,
	std::shared_ptr<ConnectionBinder> cf) :
	Pollable(socket(AF_INET6, SOCK_STREAM, 0), EPOLLIN | EPOLLET),
	conn_b(cf)
{
	bind(get_fd(), (struct sockaddr*) ip.get_sockaddr(), sizeof(sockaddr_in6));
	listen(get_fd(), backlog);
}

IPv6ListenSocket::~IPv6ListenSocket()
{
	close(get_fd());
}

/*
void IPv6ListenSocket::register_on_accept_fn(std::function<void(int)> fn, sin6_addr addr)
{
	on_accept_function = fn;
}*/

void IPv6ListenSocket::readable()
{
	sockaddr_in6 addr;
	unsigned int socklen = sizeof(addr);
	int acc_fd = accept(get_fd(), (sockaddr*) &addr, &socklen);
	if(acc_fd != -1)
		conn_b->accepted(acc_fd, addr);
	else
	{
		std::stringstream s;
		s << "accept() failed: " << errno << " - " << strerror(errno);
		throw std::runtime_error(s.str());
	}
}


void IPv6ListenSocket::writable()
{
	throw std::logic_error("not implemented!");
}


void IPv6ListenSocket::error()
{
	reactor->deregister(get_fd());
}


void IPv6ListenSocket::pri()
{
	std::cout << "IPv6ListenSocket::pri() called." << std::endl;
}


void IPv6ListenSocket::rdhup()
{
	reactor->deregister(get_fd());
}


void IPv6ListenSocket::hup()
{
	reactor->deregister(get_fd());
}



IPv6Socket::IPv6Socket(int fd) :
	Pollable(fd, EPOLLIN | EPOLLOUT | EPOLLET)
{}

IPv6Socket::~IPv6Socket()
{}

void IPv6Socket::readable()
{}

void IPv6Socket::writable()
{}

void IPv6Socket::error()
{}

void IPv6Socket::pri()
{}

void IPv6Socket::rdhup()
{
	reactor->deregister(get_fd());
}

void IPv6Socket::hup()
{
	reactor->deregister(get_fd());
}


int IPv6Socket::do_read(unsigned char * ptr, size_t count)
{
	return read(get_fd(), (void *) ptr, count);
}

int IPv6Socket::do_write(unsigned char * ptr, size_t count)
{
	return write(get_fd(), (void *) ptr, count);
}



IPv6TLSSocket::IPv6TLSSocket(int fd) :
	IPv6Socket(fd)
{
	// TODO: init TLS
}

IPv6TLSSocket::~IPv6TLSSocket()
{
	// TODO: deinit TLS
}

int IPv6TLSSocket::do_read(unsigned char * ptr, size_t count)
{
	return -1;
}

int IPv6TLSSocket::do_write(unsigned char * ptr, size_t count)
{
	return -1;
}
