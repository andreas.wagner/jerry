#ifndef _RINGBUFFER_H_
#define _RINGBUFFER_H_

class Ringbuffer
{
public:
	Ringbuffer(size_t size);
	~Ringbuffer();
	
	unsigned char * get_p_begin();
	unsigned char * get_g_begin();
	unsigned char * get_p_size();
	unsigned char * get_g_size();
	
	void p_advance(size_t s);
	void g_advance(size_t s);
	
private:
	unsigned char * begin, p_begin, g_begin;
	size_t buf_size;
};

#endif
