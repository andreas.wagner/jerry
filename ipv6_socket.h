#ifndef _IPV6_SOCKET_
#define _IPV6_SOCKET_

#include <functional>
#include <netinet/in.h>
#include <memory>

#include "pollable.h"
#include "epoll_reactor.h"

class IPv6Description
{
public:
	IPv6Description(int port);
	IPv6Description(std::string ip_addr, int port);
	~IPv6Description();

	sockaddr_in6 * get_sockaddr();
private:
	sockaddr_in6 addr;
};


class ConnectionBinder
{
public:
	ConnectionBinder();
	
	virtual void accepted(int fd, sockaddr_in6 &addr);
};



class IPv6ListenSocket : public Pollable
{
public:
	IPv6ListenSocket(
		IPv6Description & descr,
		int backlog,
		std::shared_ptr<ConnectionBinder> cf);
	~IPv6ListenSocket();
		
	virtual void readable() override;
	virtual void writable() override;
	virtual void error() override;
	virtual void pri() override;
	virtual void rdhup() override;
	virtual void hup() override;

private:
	std::shared_ptr<ConnectionBinder> conn_b;
};

class IPv6Socket : public Pollable
{
public:
	IPv6Socket(int fd);
	~IPv6Socket();
	
	virtual void readable() override;
	virtual void writable() override;
	virtual void error() override;
	virtual void pri() override;
	virtual void rdhup() override;
	virtual void hup() override;
	
	virtual int do_read(unsigned char * ptr, size_t count);
	virtual int do_write(unsigned char * ptr, size_t count);
};

class IPv6TLSSocket : public IPv6Socket
{
public:
	IPv6TLSSocket(int fd);
	~IPv6TLSSocket();
	
	virtual int do_read(unsigned char * ptr, size_t count) override;
	virtual int do_write(unsigned char * ptr, size_t count) override;
};
#endif
