#ifndef _HTTP_CONNECTION_H_
#define _HTTP_CONNECTION_H_

#include "ringbuffer.h"
#include "ipv6_socket.h"

class HTTPConnectionManager;
class HTTPConnection;

class HTTPConnectionBinder : public ConnectionBinder
{
public:
	HTTPConnectionBinder(
		std::shared_ptr<HTTPConnectionManager>,
		bool is_https);
	~HTTPConnectionBinder();
	
	virtual void accepted(int fd, sockaddr_in6 &addr) override;
	
private:
	std::shared_ptr<HTTPConnectionManager> mgr;
	bool use_https;
};

class HTTPConnectionManager
{
public:
	HTTPConnectionManager();
	~HTTPConnectionManager();
	
	void accepted(std::shared_ptr<HTTPConnection> c);
	
	
private:
	std::map<int, std::shared_ptr<HTTPConnection>> connections;
};

class HTTPConnection 
{
public:
	HTTPConnection(std::shared_ptr<IPv6Socket>);
		
	bool is_secure();
	int get_fd();
	
	void set_readbuffer_size(size_t s);
	void set_writebuffer_size(size_t s);
	
private:
	enum
	{
		HTTP_INVALID,
		HTTP_10,
		HTTP_11,
		HTTP_2
	} version;
	
	enum
	{
		READING,
		WRITING,
		INVALID
	} status;
	
	std::shared_ptr<Ringbuffer> buf_in;
	std::shared_ptr<Ringbuffer> buf_out;
	size_t read_size;
	in6_addr addr;
	bool secure;
	std::shared_ptr<IPv6Socket> socket;
};

#endif
