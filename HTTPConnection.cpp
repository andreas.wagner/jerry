#include <stdexcept>
#include "HTTPConnection.h"


HTTPConnectionBinder::HTTPConnectionBinder(
	std::shared_ptr<HTTPConnectionManager> m,
	bool is_https) :
	mgr(m),
	use_https(is_https)
{
}

HTTPConnectionBinder::~HTTPConnectionBinder()
{
}

void HTTPConnectionBinder::accepted(int fd, sockaddr_in6 &addr)
{
	std::shared_ptr<HTTPConnection> sock;
	if(use_https)
	{
		std::make_shared<IPv6TLSSocket>(fd, addr.sin6_addr, use_https);
	}
	else
	{
		std::make_shared<IPv6Socket>(fd, addr.sin6_addr, use_https);
	}
	std::shared_ptr<HTTPConnection> conn =
		std::make_shared<HTTPConnection>(sock);
	mgr->accepted(conn);
}



HTTPConnectionManager::HTTPConnectionManager()
{
}

HTTPConnectionManager::~HTTPConnectionManager()
{
}

void HTTPConnectionManager::accepted(std::shared_ptr<HTTPConnection> c)
{
	connections[c->get_fd()] = c;
}


HTTPConnection::HTTPConnection(std::shared_ptr<IPv6Socket> s) :
	socket(s),
	version(HTTP_INVALID),
	status(READING),
	buf_in(std::make_shared<Ringbuffer>(16*1024)),
	buf_out(std::make_shared<Ringbuffer>(16*1024))
{}

int HTTPConnection::get_fd()
{
	return socket->get_fd();
}

void HTTPConnection::readable()
{
	switch(version)
	{
	case HTTP_10:
	case HTTP_11:
		switch(status)
		{
		case READING:
			int n = do_read(buf_in->p_begin(), buf_in->p_size());
			if(n > 0)
				buf_in->p_advance(n);
			else if(errno != EAGAIN && errno != EINTR)
			{
				std::stringstream s;
				s << "HTTPConnection::readable(): "
					<< errno << " - " << strerror(errno);
				throw std::runtime_error(s.str());
			}
			break;
		case WRITING:
			break;
		default:
			break;
		}
		break;
	case HTTP_2:
		switch(status)
		{
			
		}
	default:
	}
	
}

void HTTPConnection::writable()
{
	switch(version)
	{
	case HTTP_10:
	case HTTP_11:
		switch(status)
		{
		case READING:
			break;
		case WRITING:
			int n = do_write(buf_out->g_begin(), buf_out->g_size());
			if(n > 0)
				buf_out->g_advance(n);;
			else if(errno != EAGAIN && errno != EINTR)
			{
				std::stringstream s;
				s << "HTTPConnection::writable(): "
					<< errno << " - " << strerror(errno);
				throw std::runtime_error(s.str());
			}
			break;
		default:
			break;
		}
		break;
	case HTTP_2:
		switch(status)
		{
			
		}
	default:
	}
}

void HTTPConnection::error()
{}

void HTTPConnection::pri()
{}

void HTTPConnection::rdhup()
{
	
}

void HTTPConnection::hup()
{}

void HTTPConnection::set_readbuffer_size(size_t s)
{
	buf_in = std::make_shared<Ringbuffer>(s)
}

void HTTPConnection::set_writebuffer_size(size_t s)
{
	buf_out = std::make_shared<Ringbuffer>(s)
}
