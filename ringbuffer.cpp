#include "ringbuffer.h"

Ringbuffer::Ringbuffer(size_t size) :
	buf_size(size),
	begin(new unsigned char[size]),
	p_begin(begin),
	g_begin(begin)
{
}

Ringbuffer::~Ringbuffer()
{
	delete[] begin;
}

unsigned char * Ringbuffer::get_p_begin()
{
	return p_begin;
}

unsigned char * Ringbuffer::get_g_begin()
{
	return g_begin();
}

/*
  +---- begin
  |    +----g_begin
  |    |   +--p_begin
  |    |   |           
  |    |   |               +--begin+buf_size
  |    |   |               |
 ##################################
*/
/*
  +---- begin
  |    
  |       +--p_begin
  |       |           +---- g_begin
  |       |           |    +--begin+buf_size
  |       |           |    |
 ##################################
*/
/**
 * Intended for count-parameter of read()- and write()-calls.
 * */
unsigned char * Ringbuffer::get_p_size()
{
	if(g_begin > p_begin)
		return g_begin - p_begin;
	else
		return p_begin - (begin + buf_size);
}

/**
 * Intended for count-parameter of read()- and write()-calls.
 * */
unsigned char * Ringbuffer::get_g_size()
{
	if(p_begin > g_begin)
		return p_begin - g_begin;
	else
		return g_begin + (begin + buf_size);
}

void Ringbuffer::p_advance(size_t s)
{
	unsined char * ptr = p_begin + s;
	if(ptr > begin + buf_size)
		throw std::logic_error("advancing beyond Ringbuffer-end");
	else if (ptr == begin + buf_size)
		p_begin = begin;
	else
		p_begin = ptr;
}

void Ringbuffer::g_advance(size_t s)
{
	unsined char * ptr = g_begin + s;
	if(ptr > begin + buf_size)
		throw std::logic_error("advancing beyond Ringbuffer-end");
	else if (ptr == begin + buf_size)
		g_begin = begin;
	else
		g_begin = ptr;
}
